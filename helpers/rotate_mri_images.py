import nibabel as nib
import numpy as np
import cv2
import csv
import os
from scipy import misc  # , ndmiage
from cupyx.scipy import ndimage  # for Rotation without CUDA on the CPU, use ndimage from scipy (above)
import \
    cupy as cp  # This is used for converting numpy arrays into cupy arrays. This can be removed if the scipy rotation is used
import random

survival_file_path = 'data/MICCAI_BraTS2020_TrainingData/survival_info.csv'
inflated_file_path = 'data/MICCAI_BraTS2020_TrainingData/survival_info_inflated.csv'
data_path = 'data/MICCAI_BraTS2020_TrainingData/'
number_of_inflations = 10
max_rotation_angles = 40.0  # in degrees. A random variable with ranges [-max_rotation_angles, max_rotation_angles] will be created


def rotate(img, roll, pitch, yaw):
    new_img = cp.asarray(img)
    # use spline order = 0 to receive the same value distributions/histogram as in the input
    new_img = ndimage.rotate(new_img, roll, axes=(0, 2), reshape=False, order=0, cval=0)
    new_img = ndimage.rotate(new_img, pitch, axes=(1, 2), reshape=False, order=0, cval=0)
    new_img = ndimage.rotate(new_img, yaw, axes=(0, 1), reshape=False, order=0, cval=0)
    return cp.asnumpy(new_img)


def multiplicateLine(output_file, row):
    key = row[0]
    age = row[1]
    days = row[2]
    resection = row[3]

    sample_path = data_path + key
    modalities = os.listdir(sample_path)
    modalities.sort()

    for i in range(number_of_inflations):  # create x new inflations
        new_key = key + "_" + '{:03d}'.format(i + 1)
        print("Creating sample " + new_key)
        new_sample_path = data_path + new_key

        if not os.path.exists(new_sample_path):  # create new folder
            try:
                os.mkdir(new_sample_path)
            except OSError:
                print("Creation of the directory %s failed" % new_sample_path)
        else:
            output_file.write(new_key + "," + age + "," + days + "," + resection + "\n")
            continue

        angle_roll = random.uniform(-max_rotation_angles, max_rotation_angles)
        angle_pitch = random.uniform(-max_rotation_angles, max_rotation_angles)
        angle_yaw = random.uniform(-max_rotation_angles, max_rotation_angles)

        for j in range(len(modalities)):  # load all given modalities/MRI types
            nii_path = sample_path + '/' + modalities[j]
            img = nib.load(nii_path)
            image_data = img.get_fdata()

            clipped_img = rotate(image_data, angle_roll, angle_pitch, angle_yaw)
            clipped_img = nib.Nifti1Image(clipped_img, img.affine, img.header)

            file_modality = "_flair"
            if (modalities[j].find('seg.nii') != -1):
                file_modality = "_seg"
            elif (modalities[j].find('t1.nii') != -1):
                file_modality = "_t1"
            elif (modalities[j].find('t1ce.nii') != -1):
                file_modality = "_t1ce"
            elif (modalities[j].find('t2.nii') != -1):
                file_modality = "_t2"

            nib.save(clipped_img, new_sample_path + "/" + new_key + file_modality + ".nii.gz")

        output_file.write(new_key + "," + age + "," + days + "," + resection + "\n")


def readDataset():
    f = open(inflated_file_path, "w")
    f.write("Brats20ID,Age,Survival_days,Extent_of_Resection\n")
    with open(survival_file_path, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        first = True
        for row in csv_reader:
            if first == True:
                first = False
                continue

            key = row[0]
            age = row[1]
            days = row[2]
            resection = row[3]
            f.write(key + "," + age + "," + days + "," + resection + "\n")

            multiplicateLine(f, row)
    f.close()


readDataset()

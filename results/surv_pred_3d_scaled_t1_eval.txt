Median:   2310.52466 
Mean:  19370.85081 
Standard SE:  68774.37264
SpearmanR - Correlation:    0.93970 | pValue:    0.00000
Final Accuracy: 0.855098389982111; MSE: [[19370.854]]
Num Class correct samples: T1 179.0, T2: 129.0, T3: 170.0
Num Class All samples: T1 211, T2: 171, T3: 177
Class accuracies: T1 0.8483412322274881, T2: 0.7543859649122807, T3: 0.96045197740113

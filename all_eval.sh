#
./eval_surv_pred.sh regression_3d t1
./eval_surv_pred.sh regression_3d t1ce
./eval_surv_pred.sh regression_3d t2
./eval_surv_pred.sh regression_3d flair
#
./eval_surv_pred.sh regression_3dcnn t1
./eval_surv_pred.sh regression_3dcnn t1ce
./eval_surv_pred.sh regression_3dcnn t2
./eval_surv_pred.sh regression_3dcnn flair
#
./eval_surv_pred.sh regression_3d_scaled t1
./eval_surv_pred.sh regression_3d_scaled t1ce
./eval_surv_pred.sh regression_3d_scaled t2
./eval_surv_pred.sh regression_3d_scaled flair
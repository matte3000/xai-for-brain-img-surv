import nibabel as nib
import cv2
from scipy import ndimage

import helpers.rotate_mri_images as hr
import helpers.image_operations as hi
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from skimage.transform import rescale


def rotate(img, roll, pitch, yaw):
    # use spline order = 0 to receive the same value distributions/histogram as in the input
    new_img = np.array(img.dataobj)
    new_img = ndimage.rotate(new_img, roll, axes=(0, 2), reshape=False, order=0, cval=0)
    new_img = ndimage.rotate(new_img, pitch, axes=(1, 2), reshape=False, order=0, cval=0)
    new_img = ndimage.rotate(new_img, yaw, axes=(0, 1), reshape=False, order=0, cval=0)
    return np.array(new_img)


FOLDER = "fig/"
angle_roll = 20
angle_pitch = 20
angle_yaw = 20

image = nib.load("data/MICCAI_BraTS2020_TrainingData/BraTS20_Training_005/BraTS20_Training_005_flair.nii.gz")
image_data = image.get_fdata()

# origin
fig, (ax11, ax12, ax13) = plt.subplots(3, 1, figsize=(5, 18))
fig.suptitle('Original', fontsize=16)
sns.heatmap(np.rot90(image_data[120, :, :]), ax=ax11, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(image_data[:, 120, :]), ax=ax12, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(image_data[:, :, 77]), ax=ax13, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
plt.savefig(FOLDER + "x1_original.png")
plt.close(fig)

# Rotateds
rotated_max = rotate(image, angle_roll, angle_pitch, angle_yaw)
rotated_max = nib.Nifti1Image(rotated_max, image.affine, image.header)
img_data = rotated_max.get_fdata()

fig, (ax11, ax12, ax13) = plt.subplots(3, 1, figsize=(5, 18))
fig.suptitle('Rotated', fontsize=16)
sns.heatmap(np.rot90(img_data[120, :, :]), ax=ax11, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(img_data[:, 120, :]), ax=ax12, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(img_data[:, :, 77]), ax=ax13, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
plt.savefig(FOLDER + "x2_rotated.png")
plt.close(fig)

nib.save(rotated_max, FOLDER + "rotated.nii.gz")

# standard
standard = hi.standardize(img_data)
standard_save = nib.Nifti1Image(standard, image.affine, image.header)
nib.save(standard_save, FOLDER + "standardized.nii.gz")

fig, (ax11, ax12, ax13) = plt.subplots(3, 1, figsize=(5, 18))
fig.suptitle('Standardized', fontsize=16)
sns.heatmap(np.rot90(standard[120, :, :]), ax=ax11, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(standard[:, 120, :]), ax=ax12, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(standard[:, :, 77]), ax=ax13, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
plt.savefig(FOLDER + "x3_standardized.png")
plt.close(fig)

rescaled = rescale(standard, 0.5, anti_aliasing=False)
fig, (ax11, ax12, ax13) = plt.subplots(3, 1, figsize=(5, 18))
fig.suptitle('Rescaled', fontsize=16)
sns.heatmap(np.rot90(rescaled[60, :, :]), ax=ax11, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(rescaled[:, 60, :]), ax=ax12, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
sns.heatmap(np.rot90(rescaled[:, :, 35]), ax=ax13, xticklabels=[], yticklabels=[], cbar=False, cmap='Greys_r',
            square=True)
plt.savefig(FOLDER + "x4_rescaled.png")
plt.close(fig)

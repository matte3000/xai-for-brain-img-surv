# Setup

- This repository requires [Python >= 3.6](https://wiki.ubuntuusers.de/Python/) with [pip](https://wiki.ubuntuusers.de/pip/), [CUDA >= 11](https://developer.nvidia.com/cuda-toolkit-archive) and [CuDNN SDK](https://developer.nvidia.com/cudnn) installed. CUDA and CuDNN can be installed together via apt (`sudo apt install libcudnn8`).

- install all required python dependencies. Therefore, in the root directory of the repository, execute: 

`pip3 install -r requirements.txt`

- Prepare your dataset. Optional arguments are:
  - `-h, --help`                - show brief help
  - `-i, --inflate`             - Inflate the original dataset by rotating the given MRI images

`./prepare_trainingset.sh [-h|--help] [-i|--inflate]`


# Survival Prediction - Training and Evaluation

- To train the survival prediction with regression on the BraTs dataset, execute:

`./train_surv_pred [-h] <model_name> [arguments]`

- To evaluate the survival prediction with regression on the BraTs dataset, execute:

`./eval_surv_pred [-h] <model_name> [arguments]`

- To validate the survival prediction with regression on the BraTs dataset and generate a csv file for validation on the [Penn Imaging Portal](https://ipp.cbica.upenn.edu/), execute:

`./val_surv_pred [-h] <model_name> [arguments]`

- The following model names for `<model_name>` are currently available:
  - `regression_l5` - Takes one centered layer of every MRI image type (including the ground truth from the Segmentation layer) and trains a regression onto it (output is normalized [0,1]).
  - `regression_3d <mri_type>` - Takes the 3D MRI image of type `<mri_type>` as input and trains a regression onto it (output is normalized [0,1]).  Valid types for `<mri_type>` are: flair, t1, t1ce, t2
  - `regression_3d_scaled <mri_type>` - Takes a scaled 3D MRI image (half the size of the original) of type `<mri_type>` as input and trains a regression onto it (output is normalized [0,1]).  Valid types for `<mri_type>` are: flair, t1, t1ce, t2. 
  - `regression_3dcnn <mri_type>` - Takes a scaled 3D MRI image (half the size of the original) of type `<mri_type>` as input and trains a regression onto it using 3D convolutional feature detectors (output is normalized [0,1]).  Valid types for `<mri_type>` are: flair, t1, t1ce, t2. 
  
- The following optinal arguments are implemented:
  -  `[-h]` - Prints a short help
  -  `[-c]` - Continues training on the existing .h5 model (if it exists and only for training)



# Troubleshooting

- If there is an error while installing the Pyhton dependencies, try upgrading pip:

`pip3 install --upgrade pip`

`python3 -m pip install --upgrade setuptools`

- Encountered Errors:

`ERROR: launchpadlib 1.10.13 requires testresources, which is not installed.`

Fix: `sudo apt install python3-testresources`

# SHAP

To create shap validation images, use the `shap_test.py` script and adapt the required fields for file and network selection.

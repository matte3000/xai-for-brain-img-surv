Median:  29713.49805 
Mean: 171695.31906 
Standard SE:  322903.00660
SpearmanR - Correlation:    0.33663 | pValue:    0.00000
Final Accuracy: 0.4472271914132379; MSE: [[171695.34]]
Num Class correct samples: T1 112.0, T2: 60.0, T3: 78.0
Num Class All samples: T1 224, T2: 179, T3: 156
Class accuracies: T1 0.5, T2: 0.33519553072625696, T3: 0.5

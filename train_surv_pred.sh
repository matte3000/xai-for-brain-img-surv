#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Error: Model missing."
  echo "Usage: ./train_surv_pred.sh <model_name> [arguments]"
  echo ""
  echo "e.g.: ./train_surv_pred.sh regression_l5"
  echo "Currently available models are : regression_l5, regression_3d, regression_3d_scaled, regression_3d_scaled_v1"
  
  exit
fi

model_name=$1
shift

python3 models/$model_name/train_model.py $@

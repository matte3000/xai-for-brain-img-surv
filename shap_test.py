import json

import matplotlib.colors
import shap
import numpy as np
from tensorflow.keras.models import Model, load_model
import sys
import random
import os
import csv
import nibabel as nib
from skimage.transform import resize, rescale
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt
import helpers.file_operations as foperation
import seaborn as sns
import pandas as pd
from tqdm import tqdm

tf.compat.v1.disable_v2_behavior()
tf.get_logger().setLevel('ERROR')

survival_file_path = 'data/MICCAI_BraTS2020_TrainingData/test_set_eval.csv'
training_data_path = 'data/MICCAI_BraTS2020_TrainingData'
result_folder = "results/"
model = "surv_pred_"
figpath = 'fig'

csvdata = pd.read_csv("data/MICCAI_BraTS2020_TrainingData/test_set_eval.csv")
debug = True

mpl.rcParams['figure.dpi'] = 600


def slicer_vis(layar_info, axes, shap_values, input_to_model, i):
    brain_z = np.rot90(np.asarray(input_to_model[i][:, :, layar_info[2]], dtype=np.float64))
    shap_z = np.rot90(np.asarray(shap_values[0][0][i][:, :, layar_info[2]]))

    brain_x = np.rot90(np.asarray(input_to_model[i][layar_info[0], :, :], dtype=np.float64))
    shap_x = np.rot90(np.asarray(shap_values[0][0][i][layar_info[0], :, :]))

    brain_y = np.rot90(np.asarray(input_to_model[i][:, layar_info[1], :], dtype=np.float64))
    shap_y = np.rot90(np.asarray(shap_values[0][0][i][:, layar_info[1], :]))

    shap_full = np.asarray(shap_values[0][0][i][:, :, :])

    # needed for a scale so we can view shap values a lot better since shap values are not in a defined range
    shap_range = max(abs(shap_full.max()), abs(shap_full.min()))

    sns.heatmap(brain_z, ax=axes[0], xticklabels=[], yticklabels=[], cbar=False, cmap='Greys', square=True)
    heat_z = sns.heatmap(shap_z, ax=axes[0], xticklabels=[], yticklabels=[], cmap='seismic', alpha=0.5,
                norm=matplotlib.colors.SymLogNorm(linthresh=0.03, vmin=-shap_range / 4, vmax=shap_range / 4),
                square=True)
    # heat_z.figure.axes[-1].plot(0.5, 0.001, "w.")

    sns.heatmap(brain_x, ax=axes[1], xticklabels=[], yticklabels=[], cbar=False, cmap='Greys', square=True)
    sns.heatmap(shap_x, ax=axes[1], xticklabels=[], yticklabels=[], cmap='seismic', alpha=0.5,
                norm=matplotlib.colors.SymLogNorm(linthresh=0.03, vmin=-shap_range / 4, vmax=shap_range / 4),
                square=True,
                # cbar_kws={"ticks": [0.01323194]}
                )
    # print(shap_values[0][1][i])

    sns.heatmap(brain_y, ax=axes[2], xticklabels=[], yticklabels=[], cbar=False, cmap='Greys', square=True)
    sns.heatmap(shap_y, ax=axes[2], xticklabels=[], yticklabels=[], cmap='seismic', alpha=0.5,
                norm=matplotlib.colors.SymLogNorm(linthresh=0.03, vmin=-shap_range / 4, vmax=shap_range / 4),
                square=True
                )


if len(sys.argv) < 2:
    print("Error: Argument MRI_TYPE is missing!")
    print("Usage: ./shap_eval.sh regression_3d_scaled <MRI_TYPE>. Valid types are: flair, t1, t1ce, t2")
    exit(1)

mri_type = sys.argv[1]

if mri_type != "flair" and mri_type != "t1" and mri_type != "t1ce" and mri_type != "t2":
    print("Invalid MRi type: " + mri_type + "; Valid types are: flair, t1, t1ce, t2")
    exit(1)

scaled = sys.argv[3]

model_name = sys.argv[2]
prediction_model_path = result_folder + 'surv_pred_' + model_name
prediction_model_path = prediction_model_path + "_" + mri_type
prediction_model_file = prediction_model_path + ".h5"
surv_model = load_model(prediction_model_file)

print(prediction_model_file)

if not os.path.isfile(prediction_model_file):
    print("Could not find model file check mri type and model")
    exit(1)

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

# if "scaled" not in model_name:
#     scaled = False
print(scaled)

input_to_model, age_list, cnt = foperation.load_model_images(
    survival_file_path,
    mri_type,
    x=120, y=120, z=78,
    training_data_path=training_data_path,
    count=29,
    scaled=scaled
)
e = shap.DeepExplainer(surv_model, [input_to_model, age_list])

shap_values = e.shap_values([input_to_model, age_list])

save_path = figpath + "/" + model_name + "/" + mri_type
if not os.path.isdir(save_path):
    os.makedirs(save_path, exist_ok=True)

coordinates = [
    # [115, 115, 75],
    # [100, 100, 65],
    # [95, 95, 55],
    [85, 85, 45],
    # [75, 75, 35],
    [65, 65, 25],
    # [55, 55, 15]
]

# shape_z = 77
# for shape in range(119, -1, -1):
#     if shape % 4 > 0:
#         coordinates.append([shape, shape, shape_z])
#         shape_z = shape_z - 2


for i in tqdm(range(cnt)):
    fig, axes = plt.subplots(nrows=len(coordinates), ncols=3, figsize=(12, 6 * len(coordinates)))
    # fig.suptitle(f"{csvdata['Brats20ID'][i]} | {prediction_model_path} | {csvdata['Extent_of_Resection'][i]}",
    #              fontsize=16)
    for axe, coordinate in zip(axes, coordinates):
        slicer_vis(coordinate, axe, shap_values, input_to_model, i)
    # cb = fig.colorbar(cax=axes[0, 0])
    # cb.ax.plot(0.5, shap_values[0][1][i][0], 'w.')

    plt.savefig(f"{save_path}/{csvdata['Brats20ID'][i]}_{csvdata['Extent_of_Resection'][i]}.png")
    plt.close(fig)

    shap_age_range = max(abs(shap_values[0][1].max()), abs(shap_values[0][1].min()))
    shap_age = shap_values[0][1][i][0]

    a = np.array([[-shap_age_range, shap_age_range]])
    fig_age = plt.figure(figsize=(9, 1.5))
    img = plt.imshow(a, cmap="seismic")
    plt.gca().set_visible(False)
    cax = plt.axes([0.1, 0.2, 0.8, 0.6])
    cbar = plt.colorbar(orientation="horizontal", cax=cax, ticks=[shap_age])
    cbar.ax.set_xticklabels(["Age Shap Value"])
    # cbar.ax.tick_params(axis="x", direction="in", labeltop="on")
    data = {
        "min_shap_age": -shap_age_range,
        "max-shap-age": shap_age_range,
        "shap-age": shap_age
    }

    with open(f"{save_path}/{csvdata['Brats20ID'][i]}_{csvdata['Extent_of_Resection'][i]}_age.json", "w") as fp:
        json.dump(data, fp)

    # os.makedirs(f"{save_path}/{csvdata['Brats20ID'][i]}_{csvdata['Extent_of_Resection'][i]", exist_ok=True)
    plt.savefig(f"{save_path}/{csvdata['Brats20ID'][i]}_{csvdata['Extent_of_Resection'][i]}_age.png")
    plt.close(fig_age)


test = True

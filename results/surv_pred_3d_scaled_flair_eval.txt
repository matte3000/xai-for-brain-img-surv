Median:   1722.32605 
Mean:  12619.51381 
Standard SE:  46454.13238
SpearmanR - Correlation:    0.97370 | pValue:    0.00000
Final Accuracy: 0.8783542039355993; MSE: [[12619.516]]
Num Class correct samples: T1 192.0, T2: 130.0, T3: 169.0
Num Class All samples: T1 227, T2: 161, T3: 171
Class accuracies: T1 0.8458149779735683, T2: 0.8074534161490683, T3: 0.9883040935672515

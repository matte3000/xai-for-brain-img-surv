import matplotlib.pyplot as plt
import numpy as np
from decimal import Decimal

shap_range = 0.06949334177749777
shap_age = 0.0006645360942222451
text_neg = '%.2E' % Decimal(-shap_range)
text = '%.2E' % Decimal(-shap_range)

a = np.array([[-shap_range, shap_range]])
plt.figure(figsize=(9, 1.5))
img = plt.imshow(a, cmap="seismic")
plt.gca().set_visible(False)
cax = plt.axes([0.1, 0.2, 0.8, 0.6])
cbar = plt.colorbar(orientation="horizontal", cax=cax, ticks=[-shap_range, shap_age, shap_range])
cbar.ax.set_xticklabels(f"{text_neg}", 'Age Shap Value', text)
plt.show()

# from matplotlib import pyplot as plt
# import numpy as np
#
# # create dummy invisible image
# # (use the colormap you want to have on the colorbar)
# img = plt.imshow(np.array([[0,1]]), cmap="Oranges")
# plt.gca().set_visible(False)
#
# cb = plt.colorbar(orientation="horizontal")
#
# # add any other things you want to the figure.
# plt.show()
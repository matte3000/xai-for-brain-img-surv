#!/bin/bash

INFLATE=false

SHARE_TRAINING=0.8 # share of the training set over all survival information. The rest is assigned to test data
# handle input arguments and flags
while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "Initialize BraTs dataset for survival prediction"
      echo " "
      echo "./prepare_trainingset.sh [-h|--help] [-i|--inflate]"
      echo " "
      echo "options:"
      echo "-h, --help                show brief help"
      echo "-i, --inflate             Inflate the original dataset by rotating the given MRI images"
      exit 0
      ;;
    -i)
      shift
      INFLATE=true
      ;;
    --inflate)
      shift
      INFLATE=true
      ;;
    *)
      break
      ;;
  esac
done

# download zip archive if necessary
if [ ! -f "data/MICCAI_BraTS2020_TrainingData.zip" ]; then
  echo "Downloading training set... This might take a while"
  mkdir data
  cd data
  wget  https://www.cbica.upenn.edu/MICCAI_BraTS2020_TrainingData
  mv MICCAI_BraTS2020_TrainingData MICCAI_BraTS2020_TrainingData.zip
  cd ..
fi

# unzip archive if not already done
if [ ! -d "data/MICCAI_BraTS2020_TrainingData" ]; then
  cd data
  unzip MICCAI_BraTS2020_TrainingData.zip
  # remove invalid line from survival data
  sed -i.bak '/ALIVE (361 days later)/d' ./MICCAI_BraTS2020_TrainingData/survival_info.csv
  cd ..
fi

if [ "$INFLATE" == "true" ]; then
  echo "Inflating training data..."
  python3 helpers/rotate_mri_images.py
  SURVIVAL_INFO_FILEPATH="MICCAI_BraTS2020_TrainingData/survival_info_inflated.csv"
else
  SURVIVAL_INFO_FILEPATH="MICCAI_BraTS2020_TrainingData/survival_info.csv"
fi



# create training and test set
cd data
echo "Creating training and test set with distribution $SHARE_TRAINING..."
echo "Brats20ID,Age,Survival_days,Extent_of_Resection" > MICCAI_BraTS2020_TrainingData/training_set.csv
echo "Brats20ID,Age,Survival_days,Extent_of_Resection" > MICCAI_BraTS2020_TrainingData/test_set.csv
# randomly read survival information and assign it to either training or test set
while read p; do
  # ignore first line
  if [[ "$p" == 'Brats20ID,Age,Survival_days,Extent_of_Resection' ]]; then
    continue
  fi
  limit=$(echo "scale=8; $RANDOM/32768" | bc )
  if (( $(echo "$limit <= $SHARE_TRAINING" |bc -l) )); then
    echo "$p" >> MICCAI_BraTS2020_TrainingData/training_set.csv
  else
    echo "$p" >> MICCAI_BraTS2020_TrainingData/test_set.csv
  fi
done < <(shuf $SURVIVAL_INFO_FILEPATH)


